const BG_COLOR = 'rgba(0,0,0,0.3)'
const LINE_COLOR = 'hsl(120,100%,50%)'
const LINE_WIDTH = 2
// const BUFFER_SIZE = 1 << Math.ceil(Math.log2(44100 / 60))
const BUFFER_SIZE = 1 << Math.floor(Math.log2(44100 / 60))
const DELAY = 1024
const MAX_QUEUED_DRAWS = 16
const COLOR_INTERVAL = 1
const A4_PHI = 440 / 44100

let prevX = 0
let prevY = 0
let prevAngle = 0
function testDraw (ctx, w, h, data) {
  const k = Math.min(w, h)
  const cx = w > h ? (w - h) / 2 : 0
  const cy = h > w ? (h - w) / 2 : 0
  const N = data[0].length
  ctx.fillStyle = BG_COLOR
  ctx.strokeStyle = LINE_COLOR
  ctx.lineWidth = LINE_WIDTH

  ctx.fillRect(0, 0, w, h)

  // ctx.strokeStyle = color
  ctx.beginPath()

  ctx.moveTo(cx + k * (0.5 + 0.5 * prevX), cy + k * (0.5 + 0.5 * prevY))

  let x, y
  for (let i = 0; i < N; i++) {
    x = data[1][i]
    y = data[0][i]
    ctx.lineTo(cx + k * (0.5 + 0.5 * x), cy + k * (0.5 + 0.5 * y))
    prevX = x
    prevY = y
  }

  // for (let i = 0; i < N; i++) {
  //   let x = i / N
  //   let y = data[0][i]
  //   // ctx.strokeStyle = `hsl(180,100%,50%)`
  //   ctx.lineTo(cx + k * (0.5 + 0.5 * x), cy + k * (0.5 + 0.5 * y))
  // }

  // for (let i = 0; i < N; i++) {
  //   let y = i / N
  //   let x = data[1][i]
  //   // ctx.strokeStyle = `hsl(180,100%,50%)`
  //   ctx.lineTo(cx + k * (0.5 + 0.5 * x), cy + k * (0.5 + 0.5 * y))
  // }

  // {
  //   let x0 = data[0][0]
  //   let y0 = data[1][0]
  //   let x1 = data[0][1]
  //   let y1 = data[1][1]
  //   let angle = Math.atan2(y, x)
  //   let delta = Math.atan2(y1, x1) - Math.atan2(y0, x0)
  //   delta = Math.abs(delta / (2 * Math.PI))
  //   phi = 0
  //   if (delta !== 0) {
  //     phi = 360 * (Math.log2(delta) % 1)
  //   }
  //   ctx.strokeStyle = `hsl(${phi},100%,50%)`
  // }
  ctx.stroke()


  // const drawWave = (data, color) => {
  //   ctx.strokeStyle = color
  //   ctx.beginPath()

  //   ctx.moveTo(0, h * (0.5 + 0.5 * data[0]))

  //   for (let i = 1; i < data.length; i++) {
  //     ctx.lineTo(w / data.length * i, h * (0.5 + 0.5 * data[i]))
  //   }

  //   ctx.stroke()

  // }

  // drawWave(data[0], '#8f8')
  // drawWave(data[1], '#88f')
  // drawWave(data[2], '#f0f')
  // drawWave(data[3], '#ff0')
}

function getMic (audio) {
  return new Promise((resolve, reject) => {
    const options = {
      video:false,
      audio:{
        mandatory:[],
        optional:[{
          echoCancellation:false
        }, {
          googEchoCancellation: false,
        }, {
          googAutoGainControl: false,
        }, {
          googNoiseSuppression: false,
        }, {
          googHighpassFilter: false
        }]
      }
    }
    const onGetUserMedia = (stream) =>
      resolve(audio.createMediaStreamSource(stream))

    navigator.getUserMedia(options, onGetUserMedia, reject)
  })
}

function initCanvas (draw) {
  const canvas = document.getElementById('scope')
  const ctx = canvas.getContext('2d')

  let drawing = 0
  const redraw = (data) => {
    if (drawing > MAX_QUEUED_DRAWS) return
    drawing++
    requestAnimationFrame(() => {
      draw(ctx, canvas.width, canvas.height, data)
      drawing = 0
    })
  }

  const onResize = () => {
    canvas.width = window.innerWidth
    canvas.height = window.innerHeight
    // redraw()
  }
  window.addEventListener('resize', onResize)
  onResize()

  return redraw
}

window.onload = () => {
  const redraw = initCanvas(testDraw)

  // let i = 0
  // let j = 0
  onProcess = (e) => {
    // i++
    // if (i % 10 > 0) return
    // redraw(e.inputBuffer.getChannelData(j))
    // j = (j + 1) % 4
    redraw([
      e.inputBuffer.getChannelData(0),
      e.inputBuffer.getChannelData(2)
    ])
  }

  const audio = new AudioContext()
  getMic(audio)
    .then((input) => {
      const processor = new QuadProcessor(audio, BUFFER_SIZE, onProcess)
      processor.setDelay(DELAY)
      processor.connectToSource(input)
    })
    .catch((e) => {
      alert('audio setup failed')
      console.error(e)
    })
}
